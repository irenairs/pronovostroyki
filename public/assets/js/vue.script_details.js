//Vue.config.devtools = true;

var tokens = {
	'#': { pattern: /\d/ },
  X: { pattern: /[0-9a-zA-Z]/ },
  S: { pattern: /[a-zA-Z]/ },
  A: { pattern: /[a-zA-Z]/, transform: v => v.toLocaleUpperCase() },
  a: { pattern: /[a-zA-Z]/, transform: v => v.toLocaleLowerCase() },
  '!': { escape: true }
}
function applyMask (value, mask, masked = true) {
  value = value || ""
  var iMask = 0
  var iValue = 0
  var output = ''
  while (iMask < mask.length && iValue < value.length) {
    cMask = mask[iMask]
    masker = tokens[cMask]
    cValue = value[iValue]
    if (masker) {
      if (masker.pattern.test(cValue)) {
      	output += masker.transform ? masker.transform(cValue) : cValue
        iMask++
      }
      iValue++
    } else {
      if (masked) output += cMask
      if (cValue === cMask) iValue++
      iMask++
    }
  }
  return output
}

Vue.directive('mask', {
	bind (el, binding) {
    var value = el.value
    Object.defineProperty(el, 'value', {
        get: function(){
            return value;
        },
        set: function(newValue){
          console.log(newValue)
            el.setAttribute('value', newValue)
        },
        configurable: true
    });
  }
})

Vue.component('input-mask', {
  template: `<input v-model="maskedValue" :maxlength="mask.length" :placeholder="mask">`,
  props: {
    'value': String,
    'mask': String,
    'masked': {
      type: Boolean,
      default: false
    }
  },

  data: () => ({
    currentValue: '',
    currentMask: '',
  }),
  computed: {
    maskedValue: {
    	get () {
        // fix removing mask character at the end.
        // Pressing backspace after 1.2.3 result in 1.2. instead of 1.2
        return this.value === this.currentValue ? this.currentMask
                                                : (this.currentMask = applyMask(this.value, this.mask, true))
      },

      set (newValue) {
        var currentPosition = this.$el.selectionEnd
        var lastMask = this.currentMask
        // update the input before restoring the cursor position
        this.$el.value = this.currentMask = applyMask(newValue, this.mask)
       
        if (this.currentMask.length <= lastMask.length) { // BACKSPACE
          // when chars are removed, the cursor position is already right
          this.$el.setSelectionRange(currentPosition, currentPosition)
        } else { // inserting characters
          // if the substring till the cursor position is the same, don't change position
          if (newValue.substring(0, currentPosition) == this.currentMask.substring(0, currentPosition)) {
            this.$el.setSelectionRange(currentPosition, currentPosition)
          } else { // increment 1 fixed position, but will not work if the mask has 2+ placeholders, like: ##//##
            this.$el.setSelectionRange(currentPosition+2, currentPosition+2)
          }
        }
        this.currentValue = applyMask(newValue, this.mask, this.masked)
        this.$emit('input', this.currentValue)
      }
    }
  }
})

 //Кркдитный калькулятор
var credit = new Vue({
    el: '#credit',
    
    data: {
        credit_summa: '',
        rate: '',
        timeYear: '',
        calculator__result: 0
    },
    methods:{
        rate_calculation: function(){
            if(isNaN(parseInt(credit.rate)) || isNaN(parseInt(credit.credit_summa)) || isNaN(parseInt(credit.timeYear))){
                credit.calculator__result = 0;
            } else {
                var s = parseInt(credit.credit_summa);
               // var rate = this.rate.replace(",",".");
                var p = parseFloat(this.rate);
                var y = parseInt(credit.timeYear);
                var m = p/100/12;
               
                var n = y * 12;
                
                var math = (1-Math.pow((1+m),-n));
                var x = s * (m/math);
                
                credit.calculator__result = x.toFixed(2);
            }
        },
        myFunction: function () {
		
		this.rate = this.rate.toString().replace(",",".");
		
        }
    }
 });
 

var modal_call = new Vue({
    el: '#modal_call',
    data: {
        fio: null,
        phone: '+7',
        loader: false,
        send: false,
        errorFio: false,
        errorPhone: false,
        modal_call: false,
        modal_call_result: false,
        send_text: 'Отправка...',
        cursor: 2 
    },
    methods:{
        send_call: function(name, id){
            //console.log(mortgage.income);
            modal_call.errorPhone = false;
            modal_call.errorFio = false;
            
            if(!this.phone){
               // feedback.error = true;
                
                this.errorPhone = true;
            } else if(this.phone.length != 10){
                this.errorPhone = true;
            }
            if(!this.fio){
                this.errorFio = true;
            }
            
            if(this.errorPhone || this.errorFio){
                
                setTimeout(function(){
                    modal_call.errorFio = false;
                    modal_call.errorPhone = false;
                }, 2000);
            } else {
                this.modal_close();
                this.modal_call_result = true;
                $('body').addClass('blocked');
                ym(56520265, 'reachGoal', 'np3');
                axios.post('/tocall', {
                        fio: this.fio,
                        phone: this.phone,
                        blockName: name,
                        blockId: id,
                        
                      })
                      .then(function (response) {
                          console.log(response.data);
                          modal_call.send_text = 'Мы перезвоним вам через 3 минуты';
                          setTimeout(function(){
                              modal_call.modal_close_result();
                             this.send_text = 'Отправка...'; 
                          }, 2000);
                          
                      })
                      .catch(function (error) {
                          console.log(error);
                      });
                  }

        },
        modal_close: function(){
            this.modal_call = false;
            $('body').removeClass('blocked');
        },
        modal_close_result: function(){
            this.modal_call_result = false;
            $('body').removeClass('blocked');
        },
        onloadForm: function(){
          $('#modal_call').show();
          
        },
        maskDop: function(){
            $('body').on('input, keyup, mouseup','.call__form-input', function(){
                console.log($(this).val().length);
                var position = this.selectionEnd;
                console.log(position);
                if($(this).val().length < 3 || position < 2){
                    $(this).val('+7');
                    this.selectionEnd = 3;
                }
                
            });
        }
           
       

    },
    created: function(){
        this.onloadForm();
        //this.maskDop();
    }
 });
 
 //report
 var report = new Vue({
    el: '#report',
    data: {
        fio: null,
        phone: '+7',
        email: null,
        errorFio: false,
        errorPhone: false,
        errorEmail: false,
        modal_call: false,
        modal_call_result: false,
        send_text: 'Отправка...'
    },
    methods:{
        send_call: function(name, id){
            //console.log(mortgage.income);
            report.errorPhone = false;
            report.errorFio = false;
            report.errorEmail = false;
            
            if(!this.phone){
                this.errorPhone = true;
            } else if(this.phone.length != 10){
                this.errorPhone = true;
            }
            
            if(!this.fio){
                this.errorFio = true;
            }
            
            if(!this.email){
                this.errorEmail = true;
            } else if(!report.validate(this.email)){
                this.errorEmail = true;
            }
            
            if(this.errorPhone || this.errorFio || this.errorEmail){
                
                setTimeout(function(){
                    report.errorFio = false;
                    report.errorPhone = false;
                    report.errorEmail = false;
                }, 2000);
            } else {
                this.modal_close();
                this.modal_call_result = true;
                $('body').addClass('blocked');
                ym(56520265, 'reachGoal', 'np2');  
                axios.post('/toreport', {
                        fio: this.fio,
                        phone: this.phone,
                        email: this.email,
                        blockName: name,
                        blockId: id
                      })
                      .then(function (response) {
                          console.log(response.data);
                          report.send_text = 'Отчет придет Вам на почту';
                          setTimeout(function(){
                             report.modal_close_result();
                             this.send_text = 'Отправка...'; 
                          }, 2000);
                          
                      })
                      .catch(function (error) {
                          console.log(error);
                      });
                  }
        },
        validate: function(email) {
           var result = true;
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            
            if(reg.test(email) == false) {
               
               result = false;
            } 
            return result;
         },
        modal_close: function(){
            this.modal_call = false;
            $('body').removeClass('blocked');
        },
        modal_close_result: function(){
            this.modal_call_result = false;
            $('body').removeClass('blocked');
        },
        onloadForm: function(){
          $('#report').show();
          
        },

    },
    created: function(){
        this.onloadForm();
    }
 });
 
 
 var send__inner = new Vue({
    el: '#send__inner',
    data: {
        fio: null,
        phone: '+7',
        errorFio: false,
        errorPhone: false,
        send_text: 'Отправка...'
    },
    methods:{
        send_call: function(name, id){
            //console.log(mortgage.income);
            send__inner.errorPhone = false;
            send__inner.errorFio = false;
            
            if(!this.phone){
               // feedback.error = true;
                
                this.errorPhone = true;
            } else if(this.phone.length != 10){
                this.errorPhone = true;
            }
            if(!this.fio){
                this.errorFio = true;
            }
            
            if(this.errorPhone || this.errorFio){
                
                setTimeout(function(){
                    send__inner.errorFio = false;
                    send__inner.errorPhone = false;
                }, 2000);
            } else {
                
                modal_call.modal_call_result = true;
                $('body').addClass('blocked');
                ym(56520265, 'reachGoal', 'np1'); 
                axios.post('/tocall', {
                        fio: this.fio,
                        phone: this.phone,
                        blockName: name,
                        blockId: id,
                        
                      })
                      .then(function (response) {
                          console.log(response.data);
                          modal_call.send_text = 'Мы перезвоним вам через 3 минуты';
                          setTimeout(function(){
                              send__inner.fio = null;
                              send__inner.phone = '+7';
                            modal_call.modal_close_result();
                            modal_call.send_text = 'Отправка...'; 
                          }, 2000);
                          
                      })
                      .catch(function (error) {
                          console.log(error);
                      });
                  }

        }
    }
 });
 
 //Отправка отзыва
 var feedback = new Vue({
    el: '#modal_feedback',
    data: {
        modal_call: false,
        fio: null,
        errorFio: false,
        block_id: '',
        review: null,
        errorReview: false,
        developer: 0,
        builder_id: ''
    },
    methods:{
        addReview: function(block_id, builder_id){
            feedback.developer = $('.feedback__btn.builder.feedback__btn_active').length;
            this.errorFio = false;
            this.errorReview = false;
            if(!this.fio){
                this.errorFio = true;
            }
            if(!this.review){
                this.errorReview = true;
            }
            
            if(this.errorFio || this.errorReview){
                 setTimeout(function(){
                    feedback.errorFio = false;
                    feedback.errorReview = false;
                }, 2000);
            }
            else {
                this.block_id = parseInt(block_id); 
                this.builder_id = parseInt(builder_id); 
                var fname = this.fio;
                var freview = this.review;
                var url = "/review";
                if(feedback.developer == 1){
                    url = "/review_builder";
                }
                
                feedback.modal_close();
                var date = new Date();
                var time = date.getHours()+':'+date.getMinutes();
                var elem = 
                '<div class="feedback__item">'+
                    '<div class="feedback__item-img-wrap"><img class="feedback__item-img" src="/static/images/avatar.svg" alt="Изображение"></div>'+
                    '<div class="feedback__item-inner">'+
                      '<p class="feedback__item-name">'+fname+'</p>'+
                      '<p class="feedback__item-date"><span class="feedback__item-date-d">Сегодня в '+time+'</span></p>'+
                      '<p class="feedback__item-desc">'+freview+'</p>'+
                    '</div>'+
                  '</div>';
                
                if(feedback.developer == 1){
                    $('.feedback__box.builder').prepend(elem);     
                } else {
                     $('.feedback__box.block').prepend(elem);         
                }
                
                axios.post(url, {
                    block_id: this.block_id,
                    review: this.review,
                    name: this.fio,
                    builder_id: this.builder_id
                  })
                  .then(function (response) {
                      console.log(response.data);
                      
                      feedback.review = null;
                      feedback.fio = null;
                  })
                  .catch(function (error) {
                      console.log(error);
                  });
            }
        },
        modal_close: function(){
            this.modal_call = false;
            $('body').removeClass('blocked');
        },
       /*modal_close_result: function(){
            this.modal_call_result = false;
            $('body').removeClass('blocked');
        },*/
        onloadForm: function(){
          $('#modal_feedback').show();
          
        },
    },
    created: function(){
        this.onloadForm();
    }
 });
 


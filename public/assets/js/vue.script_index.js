Vue.config.devtools = true;

var tokens = {
	'#': {pattern: /\d/},
  'S': {pattern: /[a-zA-Z]/},
  'A': {pattern: /[0-9a-zA-Z]/},
  'U': {pattern: /[a-zA-Z]/, transform: v => v.toLocaleUpperCase()},
  'L': {pattern: /[a-zA-Z]/, transform: v => v.toLocaleLowerCase()}
}
function applyMask (value, mask, masked = true) {
  value = value || ""
  var iMask = 0
  var iValue = 0
  var output = ''
  while (iMask < mask.length && iValue < value.length) {
    cMask = mask[iMask]
    masker = tokens[cMask]
    cValue = value[iValue]
    if (masker) {
      if (masker.pattern.test(cValue)) {
      	output += masker.transform ? masker.transform(cValue) : cValue
        iMask++
      }
      iValue++
    } else {
      if (masked) output += cMask
      if (cValue === cMask) iValue++
      iMask++
    }
  }
  return output
}

Vue.directive('mask', {
	bind (el, binding) {
    var value = el.value
    Object.defineProperty(el, 'value', {
        get: function(){
            return value;
        },
        set: function(newValue){
          console.log(newValue)
            el.setAttribute('value', newValue)
        },
        configurable: true
    });
  }
})

Vue.component('input-mask', {
  template: `<input v-model="maskedValue" :maxlength="mask.length" :placeholder="mask">`,
  props: {
    'value': String,
    'mask': String,
    'masked': {
      type: Boolean,
      default: false
    }
  },

  data: () => ({
    currentValue: '',
    currentMask: '',
  }),
  computed: {
    maskedValue: {
    	get () {
        // fix removing mask character at the end.
        // Pressing backspace after 1.2.3 result in 1.2. instead of 1.2
        return this.value === this.currentValue ? this.currentMask
                                                : (this.currentMask = applyMask(this.value, this.mask, true))
      },

      set (newValue) {
        var currentPosition = this.$el.selectionEnd
        var lastMask = this.currentMask
        // update the input before restoring the cursor position
        this.$el.value = this.currentMask = applyMask(newValue, this.mask)

        if (this.currentMask.length <= lastMask.length) { // BACKSPACE
          // when chars are removed, the cursor position is already right
          this.$el.setSelectionRange(currentPosition, currentPosition)
        } else { // inserting characters
          // if the substring till the cursor position is the same, don't change position
          if (newValue.substring(0, currentPosition) == this.currentMask.substring(0, currentPosition)) {
            this.$el.setSelectionRange(currentPosition, currentPosition)
          } else { // increment 1 fixed position, but will not work if the mask has 2+ placeholders, like: ##//##
            this.$el.setSelectionRange(currentPosition+1, currentPosition+1)
          }
        }
        this.currentValue = applyMask(newValue, this.mask, this.masked)
        this.$emit('input', this.currentValue)
      }
    }
  }
})

//Заявки в банк
var mortgage = new Vue({
    el: '#mortgage__wrapper',
    data: {
        fio: null,
        phone: '+7',
        loader: false,
        send: false,
        income: 50000,
        work: 2,
        error: false,
        errorText: '',
        payment: 50000,
        capital: false,
    },
    methods:{
        send_mortgage: function(name, id){
            $('.callback__modal.error .callback__description').html('');
            mortgage.error = false;
            
            if(!this.phone){
               // feedback.error = true;
                
                this.errorText += '<p>Укажите номер телефона</p>';
            } else if(this.phone.length != 10){
                
               // feedback.error = true;
                this.errorText += '<p>Укажите корректный номер телефона</p>';
            }
            if(!this.fio){
                this.errorText += '<p>Укажите свое имя</p>';
            }
            if(this.errorText != ''){
                $('.callback__modal.error .callback__description').html(this.errorText);
                $('.callback__modal.error').removeClass('modal--closed');
                console.log(this.errorText);
               
            } else {
                $('.callback__modal.message').removeClass('modal--closed');
                mortgage.loader=true;
            
                axios.post('/mortgage', {
                        fio: this.fio,
                        phone: this.phone,
                        blockName: "Заявка с главной страницы",
                        blockId: 0,
                        income: mortgage.income,
                        work: mortgage.work,
                        payment: mortgage.payment,
                        capital: (mortgage.capital) ? 1 : 0,

                      })
                      .then(function (response) {
                          console.log(response.data);
                          mortgage.loader=false;
                          mortgage.send=true;
                      })
                      .catch(function (error) {
                          console.log(error);
                      });
                  }

        }
    }
 });
 
 var to_book = new Vue({
    el: '#modal_call',
    data: {
        name: null,
        phone: '+7',
        loader: false,
        send: false,
        error: false,
        errorText: '',
        lastname: null, 
        
    },
    methods:{
        send_call: function(name, id){
            //console.log(mortgage.income);
            to_book.error = false;
            
            if(!this.phone){
               // feedback.error = true;
                
                this.errorText += '<p>Укажите номер телефона</p>';
            } else if(this.phone.length != 10){
                
               // feedback.error = true;
                this.errorText += '<p>Укажите корректный номер телефона</p>';
            }
            if(!this.name){
                this.errorText += '<p>Укажите свое имя</p>';
            }
            
            if(this.errorText != ''){
                to_book.error = true;
                setTimeout(function(){
                    to_book.error = false;
                    to_book.errorText = '';
                }, 2000);
            } else {
                to_book.loader=true;
                to_book.flatId=$("#booking").data("id");
                axios.post('/tocall', {
                        fio: this.name,
                        phone: this.phone,
                        blockName: "Заявка с главной страницы",
                        blockId: 0,
                        
                      })
                      .then(function (response) {
                          console.log(response.data);
                          to_book.loader=false;
                          to_book.send=true;
                          setTimeout(function(){
                              $('.modal-to-book').addClass('modal--closed');
                              $('.modal-to-book').removeClass('modal-to-book--active');
                              to_book.send=false;
                              to_book.error=false;
                          }, 2000);

                      })
                      .catch(function (error) {
                          console.log(error);
                      });
                  }

        }
    }
 });
 
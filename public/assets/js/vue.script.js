Vue.config.devtools = true;

var tokens = {
	'#': {pattern: /\d/},
  'S': {pattern: /[a-zA-Z]/},
  'A': {pattern: /[0-9a-zA-Z]/},
  'U': {pattern: /[a-zA-Z]/, transform: v => v.toLocaleUpperCase()},
  'L': {pattern: /[a-zA-Z]/, transform: v => v.toLocaleLowerCase()}
}
function applyMask (value, mask, masked = true) {
  value = value || ""
  var iMask = 0
  var iValue = 0
  var output = ''
  while (iMask < mask.length && iValue < value.length) {
    cMask = mask[iMask]
    masker = tokens[cMask]
    cValue = value[iValue]
    if (masker) {
      if (masker.pattern.test(cValue)) {
      	output += masker.transform ? masker.transform(cValue) : cValue
        iMask++
      }
      iValue++
    } else {
      if (masked) output += cMask
      if (cValue === cMask) iValue++
      iMask++
    }
  }
  return output
}

Vue.directive('mask', {
	bind (el, binding) {
    var value = el.value
    Object.defineProperty(el, 'value', {
        get: function(){
            return value;
        },
        set: function(newValue){
          console.log(newValue)
            el.setAttribute('value', newValue)
        },
        configurable: true
    });
  }
})

Vue.component('input-mask', {
  template: `<input v-model="maskedValue" :maxlength="mask.length" :placeholder="mask">`,
  props: {
    'value': String,
    'mask': String,
    'masked': {
      type: Boolean,
      default: false
    }
  },

  data: () => ({
    currentValue: '',
    currentMask: '',
  }),
  computed: {
    maskedValue: {
    	get () {
        // fix removing mask character at the end.
        // Pressing backspace after 1.2.3 result in 1.2. instead of 1.2
        return this.value === this.currentValue ? this.currentMask
                                                : (this.currentMask = applyMask(this.value, this.mask, true))
      },

      set (newValue) {
        var currentPosition = this.$el.selectionEnd
        var lastMask = this.currentMask
        // update the input before restoring the cursor position
        this.$el.value = this.currentMask = applyMask(newValue, this.mask)

        if (this.currentMask.length <= lastMask.length) { // BACKSPACE
          // when chars are removed, the cursor position is already right
          this.$el.setSelectionRange(currentPosition, currentPosition)
        } else { // inserting characters
          // if the substring till the cursor position is the same, don't change position
          if (newValue.substring(0, currentPosition) == this.currentMask.substring(0, currentPosition)) {
            this.$el.setSelectionRange(currentPosition, currentPosition)
          } else { // increment 1 fixed position, but will not work if the mask has 2+ placeholders, like: ##//##
            this.$el.setSelectionRange(currentPosition+1, currentPosition+1)
          }
        }
        this.currentValue = applyMask(newValue, this.mask, this.masked)
        this.$emit('input', this.currentValue)
      }
    }
  }
})



//Кркдитный калькулятор
var credit = new Vue({
    el: '#credit',
    data: {
        credit_summa: '',
        rate: '',
        timeYear: '',
        calculator__result: 0
    },
    methods:{
        rate_calculation: function(){
            if(isNaN(parseInt(credit.rate)) || isNaN(parseInt(credit.credit_summa)) || isNaN(parseInt(credit.timeYear))){
                credit.calculator__result = 0;
            } else {
                var s = parseInt(credit.credit_summa);
               // var rate = this.rate.replace(",",".");
                var p = parseFloat(this.rate);
                var y = parseInt(credit.timeYear);
                var m = p/100/12;
               
                var n = y * 12;
                
                var math = (1-Math.pow((1+m),-n));
                var x = s * (m/math);
                
                credit.calculator__result = x.toFixed(2);
            }
        },
        myFunction: function () {
		
		this.rate = this.rate.toString().replace(",",".");
		
        }
    }
 });
 
 //Отправка отзыва
 var feedback = new Vue({
    el: '#modal_feedback',
    data: {
        name: '',
        block_id: '',
        review: '',
        developer: 0,
        builder_id: ''
    },
    methods:{
        addReview: function(block_id, builder_id){
            feedback.developer = $('.feedback__toogle-link--developer.feedback__toogle-link--active').length;
            if(this.name != '' && this.review != ''){
                this.block_id = parseInt(block_id); 
                this.builder_id = parseInt(builder_id); 
                var fname = this.name;
                var freview = this.review;
                var url = "/review";
                if(feedback.developer == 1){
                    url = "/review_builder";
                }
                
                axios.post(url, {
                    block_id: this.block_id,
                    review: this.review,
                    name: this.name,
                    builder_id: this.builder_id
                  })
                  .then(function (response) {
                      console.log(response.data)
                      let arrFeedbacksTmp = [];
                      var date = new Date();
                      var time = date.getHours()+':'+date.getMinutes();
                      
                      arrFeedbacksTmp.push({avatar: '/assets/img/avatar.svg',name: fname, feedback: freview, date:'Сегодня в ' + time});
                      
                      if(feedback.developer == 1){
                         if(arrFeedbacksDeveloper){
                              for(let i = 0; i < arrFeedbacksDeveloper.length; i++){
                                  arrFeedbacksTmp.push(arrFeedbacksDeveloper[i]);
                              }
                            }

                            arrFeedbacksDeveloper = arrFeedbacksTmp;
                           // console.log(arrFeedbacks);
                            updateFeedback(arrFeedbacksDeveloper); 
                      } else {
                            if(arrFeedbacks){
                              for(let i = 0; i < arrFeedbacks.length; i++){
                                  arrFeedbacksTmp.push(arrFeedbacks[i]);
                              }
                            }

                            arrFeedbacks = arrFeedbacksTmp;
                           // console.log(arrFeedbacks);
                            updateFeedback(arrFeedbacks);
                        }
                      $('.modal-feedback').removeClass('modal-feedback--active');
                      feedback.review = '';
                      feedback.name = '';
                  })
                  .catch(function (error) {
                      console.log(error);
                  });
            }
        }
    }
 });
 
 
 
 var consultation = new Vue({
    el: '#consultation_form',
    data: {
        fio: null,
        phone: '+7',
        loader: false,
        send: false,
       
        error: false,
        errorText: ''
    },
    methods:{
        sendConsultation: function(name, id){
            consultation.error = false;
            
            if(!this.phone){
               // feedback.error = true;
                
                this.errorText += '<p>Укажите номер телефона</p>';
            } else if(this.phone.length != 10){
                
               // feedback.error = true;
                this.errorText += '<p>Укажите корректный номер телефона</p>';
            }
            if(!this.fio){
                this.errorText += '<p>Укажите свое имя</p>';
            }
            if(this.errorText != ''){
                consultation.error = true;
                setTimeout(function(){
                    consultation.error = false;
                    consultation.errorText = '';
                }, 2000);
            } else {
                consultation.loader=true;
            
                axios.post('/consultation', {
                        fio: this.fio,
                        phone: this.phone,
                        blockName: name,
                        blockId: id

                      })
                      .then(function (response) {
                          console.log(response.data);
                          consultation.loader=false;
                          consultation.send=true;
                          setTimeout(function(){
                              $('.consultation__modal').addClass('modal--closed');
                              consultation.send=false;
                              consultation.error=false;
                          }, 2000);

                      })
                      .catch(function (error) {
                          console.log(error);
                      });
                  }

        }
    }
 });
 
 
 //mortgage__wrapper
 var mortgage = new Vue({
    el: '#mortgage__wrapper',
    data: {
        fio: null,
        phone: '+7',
        loader: false,
        send: false,
        income: 50000,
        work: 2,
        error: false,
        errorText: '',
        payment: 50000,
        capital: false,
    },
    methods:{
        send_mortgage: function(name, id){
            //console.log(mortgage.income);
            mortgage.error = false;
            
            if(!this.phone){
               // feedback.error = true;
                
                this.errorText += '<p>Укажите номер телефона</p>';
            } else if(this.phone.length != 10){
                
               // feedback.error = true;
                this.errorText += '<p>Укажите корректный номер телефона</p>';
            }
            if(!this.fio){
                this.errorText += '<p>Укажите свое имя</p>';
            }
            if(this.errorText != ''){
                mortgage.error = true;
                setTimeout(function(){
                    mortgage.error = false;
                    mortgage.errorText = '';
                }, 2000);
            } else {
                mortgage.loader=true;
            
                axios.post('/mortgage', {
                        fio: this.fio,
                        phone: this.phone,
                        blockName: name,
                        blockId: id,
                        income: mortgage.income,
                        work: mortgage.work,
                        payment: mortgage.payment,
                        capital: (mortgage.capital) ? 1 : 0,

                      })
                      .then(function (response) {
                          console.log(response.data);
                          mortgage.loader=false;
                          mortgage.send=true;
                          setTimeout(function(){
                              $('.mortgage__modal').addClass('modal--closed');
                              mortgage.send=false;
                              mortgage.error=false;
                          }, 2000);

                      })
                      .catch(function (error) {
                          console.log(error);
                      });
                  }

        }
    }
 });
 
 //modal_to_book
 
 var to_book = new Vue({
    el: '#modal_to_book',
    data: {
        name: null,
        phone: '+7',
        loader: false,
        send: false,
        error: false,
        errorText: '',
        lastname: null, 
        flatId: 0,
    },
    methods:{
        send_to_book: function(name, id){
            //console.log(mortgage.income);
            to_book.error = false;
            
            if(!this.phone){
               // feedback.error = true;
                
                this.errorText += '<p>Укажите номер телефона</p>';
            } else if(this.phone.length != 10){
                
               // feedback.error = true;
                this.errorText += '<p>Укажите корректный номер телефона</p>';
            }
            if(!this.name){
                this.errorText += '<p>Укажите свое имя</p>';
            }
            
            if(this.errorText != ''){
                to_book.error = true;
                setTimeout(function(){
                    to_book.error = false;
                    to_book.errorText = '';
                }, 2000);
            } else {
                to_book.loader=true;
                to_book.flatId=$("#booking").data("id");
                axios.post('/tobook', {
                        fio: this.name,
                        phone: this.phone,
                        blockName: name,
                        blockId: id,
                        flatId: to_book.flatId,
                      })
                      .then(function (response) {
                          console.log(response.data);
                          to_book.loader=false;
                          to_book.send=true;
                          setTimeout(function(){
                              $('.modal-to-book').addClass('modal--closed');
                              $('.modal-to-book').removeClass('modal-to-book--active');
                              to_book.send=false;
                              to_book.error=false;
                          }, 2000);

                      })
                      .catch(function (error) {
                          console.log(error);
                      });
                  }

        }
    }
 });
 
 
 //modal_to_book
 
 var excursion = new Vue({
    el: '#excursion_form',
    data: {
        fio: null,
        phone: '+7',
        loader: false,
        send: false,
        error: false,
        errorText: '',
        lastname: null
    },
    methods:{
        send_excursion: function(name, id){
            //console.log(mortgage.income);
            excursion.error = false;
            
            if(!this.phone){
               // feedback.error = true;
                
                this.errorText += '<p>Укажите номер телефона</p>';
            } else if(this.phone.length != 10){
                
               // feedback.error = true;
                this.errorText += '<p>Укажите корректный номер телефона</p>';
            }
            if(!this.fio){
                this.errorText += '<p>Укажите свое имя</p>';
            }
            
            if(this.errorText != ''){
                excursion.error = true;
                setTimeout(function(){
                    excursion.error = false;
                    excursion.errorText = '';
                }, 2000);
            } else {
                excursion.loader=true;
                
                axios.post('/excursion', {
                        fio: this.fio,
                        phone: this.phone,
                        blockName: name,
                        blockId: id,
                        date:$('#excursion_form #date').text(),
                        timeex:$('#excursion_form #time').text()
                      })
                      .then(function (response) {
                          console.log(response.data);
                          excursion.loader=false;
                          excursion.send=true;
                          setTimeout(function(){
                              $('.excursion__modal').addClass('modal--closed');
                             
                              excursion.send=false;
                              excursion.error=false;
                          }, 2000);

                      })
                      .catch(function (error) {
                          console.log(error);
                      });
                  }

        }
    }
 });

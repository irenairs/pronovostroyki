var mySwiper = new Swiper(".house__slider", {
        loop: !0,
        slidesPerView: 1,
        spaceBetween: 15,
        navigation: {
            nextEl: ".house__slider-btn-next",
            prevEl: ".house__slider-btn-prev"
        },
        lazy: {
            loadPrevNext: true,
          },
        breakpoints: {
            768: {
                slidesPerView: 2,
                spaceBetween: 30
            },
            1100: {
                slidesPerView: 3,
                spaceBetween: 30
            }
        }
    })
  
var sliders = $('.main-slider-controls__slide').length;
//console.log(sliders);
var galleryTop = new Swiper('.main-slider', {
      spaceBetween: 15,
      slidesPerView: "auto",
      slideActiveClass: "activeClass",
      navigation: {
        nextEl: ".slider-w__btn-next",
        prevEl: ".slider-w__btn-prev"
      },
        loop: true,
	loopedSlides: sliders
    });
    var galleryThumbs = new Swiper('.main-slider-controls', {
      spaceBetween: 15,
      direction: "vertical",
      slidesPerView: 5,
      slideToClickedSlide: true,
      freeModeSticky: true,
      grabCursor: true,
    
      navigation: {
        nextEl: ".slider-h__btn-next",
        prevEl: ".slider-h__btn-prev"
    },
    	centerInsufficientSlides: true,
        loop: true,
	loopedSlides: galleryTop.loopedSlides
    });
    galleryTop.controller.control = galleryThumbs;
    galleryThumbs.controller.control = galleryTop;

var vidid = '';

if(typeof videosId != 'undefined'){
    vidid = videosId[0].idvid;
}
  
let link = "https://www.youtube.com/embed/"+vidid,
    btnNext = document.querySelector(".slider-w__btn-next"),
    btnPrev = document.querySelector(".slider-w__btn-prev");
btnNext.addEventListener("click", (function() {
    let e = document.querySelector(".activeClass.videoAct");
    e ? e.querySelector(".videoIframe").src = link + "?autoplay=1" : document.querySelectorAll(".videoIframe").forEach(e => {
        e.src = ""
    })
})), 
btnPrev.addEventListener("click", (function() {
    let e = document.querySelector(".activeClass.videoAct");
    e ? e.querySelector(".videoIframe").src = link + "?autoplay=1" : document.querySelectorAll(".videoIframe").forEach(e => {
        e.src = ""
    })
}));


$('.main-slider-controls__slide,.slider-h__btn-next,.slider-h__btn-prev').click(function(){
    let e = document.querySelector(".activeClass.videoAct");
    e ? e.querySelector(".videoIframe").src = link + "?autoplay=1" : document.querySelectorAll(".videoIframe").forEach(e => {
        e.src = ""
    })
});

$('body').on('click', '.plans__item', function(){
    var active = $(this).hasClass('plans__item_active');
    $('.plans__item').removeClass('plans__item_active');
    $('.plans__info').removeClass('plans__info_active');
    $('.plans__info').removeClass('plans__info_right');
    $('.plans__item-wrap').removeClass('plans__item-wrap_active');
    $('.plans__info-plan').removeClass('active');
    
    
   
    if(!active){
        $(this).addClass('plans__item_active');
        $(this).closest('.plans__item-wrap').find('.plans__info').addClass('plans__info_active');

        if($(this).closest('.plans__item-wrap').hasClass('plans__item-wrap_right')){
           // $(this).closest('.plans__item-wrap').addClass('plans__info_right');
            $(this).closest('.plans__item-wrap').find('.plans__info').addClass('plans__info_right');
        } 

        $(this).closest('.plans__item-wrap').addClass('plans__item-wrap_active');
        $(this).closest('.plans__item-wrap').find('.first_apartment').addClass('active');
        $(this).closest('.plans__item-wrap').find('.first_apartment').find('.plans__info-plan-img').attr('src', 
            $(this).closest('.plans__item-wrap').find('.first_apartment').find('.plans__info-plan-img').data("src"))
                    .removeAttr('data-src');
    }
    
});

$('body').on('click', '.plans__info-row', function(){
    
    var id = $(this).data("id");
    console.log(id);
   // $('.plans__info-row').removeClass('plans__info-row_active');
    if($(this).hasClass('plans__info-row_active') && screen.width < 1025){
        $('.plans__info-row').removeClass('plans__info-row_active');
        $('.plans__info-plan').removeClass('active');
    } else {
        $('.plans__info-row').removeClass('plans__info-row_active');
        $(this).addClass('plans__info-row_active');
            $('.plans__info-plan').removeClass('active');
            $('#info-plan_'+id).addClass('active');
            $('#info-plan-img_'+id).attr('src', $('#info-plan-img_'+id).data("src")).removeAttr('data-src');
        }
});


const benefits = () => {
    const e = document.querySelectorAll(".benefits__article-title"),
        t = document.querySelectorAll(".benefits__article");
    for (let l = 0; l < e.length; l++) e[l].addEventListener("click", () => {
        t[l].classList.toggle("benefits__article_show")
    })
    
    
};

benefits();
const features = () => {
    const e = document.querySelectorAll(".feature__item-title"),
        t = document.querySelectorAll(".feature__item");
    for (let l = 0; l < e.length; l++) e[l].addEventListener("click", () => {
        t[l].classList.toggle("feature__item_show")
    })
};
features();
/*const videoDesk = () => {
    const e = document.querySelectorAll(".desc__content-video"),
        t = document.querySelectorAll(".desc__content-video-youtube"),
        l = document.querySelectorAll(".desc-youtube");
    for (let l = 0; l < e.length; l++) e[l].addEventListener("click", () => {
        t[l].classList.add("desc__content-video-youtube_active")
    });
    e[0].addEventListener("click", () => {
        l[0].src = "https://www.youtube.com/embed/zkfXyNRnIqU?autoplay=1&loop=1&&playlist=Video_ID"
    }), e[1].addEventListener("click", () => {
        l[1].src = "https://www.youtube.com/embed/tfsNHeVVhdQ?autoplay=1&loop=1&&playlist=Video_ID"
    })
};
videoDesk();*/

///vidid = videosId[0].idvid;

$('body').on('click', '.desc__content-video', function(){
    $(this).find('.desc__content-video-youtube').addClass('desc__content-video-youtube_active');
    var id = $(this).data("id");
    var link = "https://www.youtube.com/embed/"+videosId[id].idvid+"?autoplay=1&loop=1&&playlist=Video_ID";
    $(this).find('.desc-youtube').attr('src', link);
    
});

/*const popup = () => {
    const e = document.querySelector(".call"),
        t = document.querySelectorAll(".popup-show"),
        l = document.querySelector(".call__form-btn-close"),
        c = document.body;
    for (let l = 0; l < t.length; l++) t[l].addEventListener("click", () => {
        e.classList.add("call_active"), c.classList.add("blocked")
    });
    l.addEventListener("click", () => {
        e.classList.remove("call_active"), c.classList.add("blocked")
    })
};*/

function initMap() {
    var e = {
            lat: -25.344,
            lng: 131.036
        },
        t = new google.maps.Map(document.getElementById("map"), {
            zoom: 4,
            center: e
        });
    new google.maps.Marker({
        position: e,
        map: t
    })
}
//popup(), 
/*document.addEventListener("click", (function(e) {
    let t = document.querySelector(".call_active");
    const l = document.body;
    e.target == t && (t.classList.remove("call_active"), l.classList.remove("blocked"))
})),*/



$(document).ready((function() {
    
    /*screen.width < 768 && $(".benefits__article-title").click((function() {
        $(this).next($(".benefits__article-list")).slideToggle()
    })),*/ screen.width < 1024 && $(".feature__item-title").click((function() {
        $(this).next($(".feature__item-list")).slideToggle()
    }))
    
}));

function num2str(n, text_forms) { //Склонение слов в зависимости от числа
    n = Math.abs(n) % 100;
    var n1 = n % 10;
    if (n > 10 && n < 20) {
      return text_forms[2];
    }
    if (n1 > 1 && n1 < 5) {
      return text_forms[1];
    }
    if (n1 == 1) {
      return text_forms[0];
    }
    return text_forms[2];
  }

$(document).ready(function(){
    
    if(screen.width < 1025){
        $('.plans__info-row').removeClass('plans__info-row_active');
        $('.plans__info-plan').removeClass('first_apartment');
    }
    //Цены аппартаментов
    $('body').on('click', '.review__flat-btn', function(){
        var type = $(this).data("type");
        $('.review__flat-btn').removeClass('review__flat-btn_active');
        $(this).addClass('review__flat-btn_active');
        $('.review__p-cost').addClass('hidden');
        $('#review__p-cost_'+type).removeClass('hidden');
    });
    
    
    //Ещё видео
    $('body').on('click', '.desc__content-link', function(){
        var vid = $('.desc__content-video.hidden');
        if(vid.length > 0){
            $(vid[0]).removeClass('hidden');
        } 
        if($('.desc__content-video.hidden').length == 0){
            $(this).addClass('hidden');
        }
    });
    
    //ещё документы
    $('body').on('click', '.docs__btn-more', function(){
        var docs = $('.docs__inner.hidden');
        if(docs.length > 0){
            $(docs[0]).removeClass('hidden');
        }
        if($('.docs__inner.hidden').length == 0){
            $(this).addClass('hidden');
        } else {
            var total = $(this).attr("data-total");
            var change = parseInt(total) - 3;
            $(this).attr("data-total", change);
            $('.docs__btn-more-span').text(change);
            $('.docs__btn-more-span_text').text(num2str(change, ['документ', 'документа', 'документов']));
        }
    });
    
    //ещё банки
    $('body').on('click', '.credit__btn', function(){
        var banks = $('.credit__inner.hidden');
        if(banks.length > 0){
            $(banks[0]).removeClass('hidden');
        }
        if($('.credit__inner.hidden').length == 0){
            $(this).addClass('hidden');
        } 
    });
    
    $('body').on('click', function (e) {

        if (!$(e.target).parents('.galery__item-img').length && !$(e.target).closest('.progress__gallery-list').length && !$(e.target).hasClass('build__btn-more')&&!$(e.target).closest('.expectation__photo-box').length) {
          $('.modal-photo-galery').removeClass('modal-photo-galery--active');
          galeryDestroy();
        }
      });
      
      
      //ещё отзывы
    $('body').on('click', '.feedback__btn-more', function(){
        if($(this).hasClass('builder')){
           $('.feedback__box.builder .feedback__item').removeClass('hidden').removeClass('is-desktop'); 
        } else {
            $('.feedback__box.block .feedback__item').removeClass('hidden').removeClass('is-desktop');
        }
        $(this).addClass('hidden');
    });
    
    $('body').on('click', '.feedback__btn', function(){
        $('.feedback__btn').removeClass('feedback__btn_active');
        $(this).addClass('feedback__btn_active');
        $('.feedback__box').addClass('hidden');
        $('.feedback__btn-more').addClass('hidden');
        if($(this).hasClass('builder')){
            $('.feedback__box.builder').removeClass('hidden');
            
            if($('.feedback__box.builder .hidden').length > 0){
                $('.feedback__btn-more.builder').removeClass('hidden');
            }
        } else {
            $('.feedback__box.block').removeClass('hidden');
            if($('.feedback__box.block .hidden').length > 0){
                $('.feedback__btn-more.block').removeClass('hidden');
            }
        }
    });
    
    //Формы заявок
    $('body').on('click', '.popup-show, .credit__cl-form-btn', function(){
        $('body').addClass('blocked');
        modal_call.modal_call = true;
        modal_call.fio = null;
        modal_call.phone = '+7';
    });
    
    //Отчет по продажам
    $('body').on('click', '.chart__desc-btn', function(){
        $('body').addClass('blocked');
        report.modal_call = true;
        report.fio = null;
        report.email = null;
        report.phone = '+7';
    });
    
    //Отзывы
    $('body').on('click','.feedback__btn-call', function(){
        $('body').addClass('blocked');
        feedback.modal_call = true;
        feedback.fio = null;
        feedback.review = null;
    });

    $('body').click(function(e){
        if($(e.target).hasClass('call_active')){
            modal_call.modal_call = false;
            modal_call.modal_call_result = false;
            report.modal_call = false;
            report.modal_call_result = false;
            feedback.modal_call = false;
            $('body').removeClass('blocked');
        }
    });
    
    //Скроллы
    $('body').on('click','.header__nav-link', function(){
        var el = $(this).data("skroll");
        var speed = 1000;
		
        // место скролла
        var top = $('#'+el).offset().top;
		
        $('html, body').animate({scrollTop: (top-30)}, speed);
		
	return false;
    });
    
    $('body').on('click','.header__menu-link', function(){
        var el = $(this).data("skroll");
        var speed = 1000;
		
        // место скролла
        var top = $('#'+el).offset().top;
		
        $('html, body').animate({scrollTop: (top-30)}, speed);
        
        $(this).toggleClass('header__btn-toggle');
        $('.header__menu').toggleClass('header__menu_active');
		
	return false;
    });
    
    //Верхнее меню
    $('body').on('click', '.header__btn', function(){
        $(this).toggleClass('header__btn-toggle');
        $('.header__menu').toggleClass('header__menu_active');
    });

});

function galeryDestroy() {
  if ($('.galery__slider').hasClass('slick-initialized')) {
    setTimeout(unslick, 250);

    function unslick() {
      $('.galery__slider').slick('unslick');
    }
  }
}

function initSlidersModalPhoto(slider, begin) {
  if (slider.hasClass('slick-initialized')) {
    $(slider).slick('unslick');
  }
  if (begin == undefined) {
    begin = 0;
  }

  $(slider).slick({
    arrows: false,
    initialSlide: parseInt(begin)
  });
  $('.galery__item-img img').off('click');
  $('.galery__item-img img').on('click', function () {
    $(slider).slick('slickNext');
    
  });

  $('.galery__item-img .close').off('click');
  $('.galery__item-img .close').on('click', function () {
    galeryDestroy();
    $('.modal-photo-galery').removeClass('modal-photo-galery--active');
  });
}

//Yandex Map
;(function() {
  //var descriptionTopY = $('#description').offset().top;

  let descriptionTopY;
  if ($('.credit').length) {
    descriptionTopY= $('.credit').offset().top;
  }

  $(window).bind('scroll', function () {
    if ($(this).scrollTop() >= descriptionTopY) {
      $(this).unbind('scroll');
      ymaps.ready(function () {
        var myMap = new ymaps.Map('map', {
            center: [block_latitude, block_longitude],
            zoom: 14
          }, {
            searchControlProvider: 'yandex#search'
          }),
          MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
          ),
          myPlacemark = new ymaps.Placemark(myMap.getCenter(), {}, {
            iconLayout: 'default#image',
            iconImageHref: '/assets/img/map_pin.png',
            iconImageSize: [79, 87],
            iconImageOffset: [-40, -87]
          })
        myMap.geoObjects
          .add(myPlacemark)
        myMap.behaviors
          .disable(['scrollZoom', 'rightMouseButtonMagnifier'])
          .enable('ruler');
      });

    }
  });
}) ();

//График
$(document).ready(function () {
    if($('#myChart').length > 0) {
    // let chartData = [];
    // chartData['studio'] = [63, 83, 36, 90, 43, 125 ]; //студии
    // chartData['1'] = [80, 111, 44, 90, 45, 125, 110]; // 1к
    // chartData['2'] = [45, 58, 87, 23, 95, 41, 54]; // 2к
    // chartData['3'] = [80, 88, 75, 148, 95, 125, 46]; // 3к
    // chartData['4'] = [6, 32, 5, 90, 95, 125, 110]; // 4к
    // chartData['5'] = [11, 83, 75, 90, 95, 125, 30]; //5+к
    // chartData['6'] = [11, 83, 75, 90, 95, 125, 100]; //5+к

    var ctx = document.getElementById('myChart').getContext('2d');
    // var dataLabels = ['нояб’18', 'дек’18', 'янв’19', 'фев’19', 'март’19', 'апр’19', 'май’19'];
    var data = [];
    
    for (let i = 0; i < chartData['1'].length; i++) {
      const element = chartData['1'][i];
      data.push(element);
    }

    var chart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: dataLabels,
        datasets: [{
          label: '',
          backgroundColor: 'rgba(233,70,70, 0.1)',
          borderColor: 'rgb(233,70,70)',
          borderWidth: 2,
          pointHoverBackgroundColor: 'rgb(233,70,70)',
          pointBackgroundColor: '#fff',
          spanGaps: true,
          data: data,
          pointRadius: 7,
          pointHoverRadius: 7,
          pointHitRadius: 7,

        }]
      },

      options: {
        bezierCurve : false,
        title: {
          display: false,
        },
        legend: {
          display: false
        },
        tooltips: {

          enabled: false,
          custom: function (tooltipModel) {
            var tooltipEl = document.getElementById('chartjs-tooltip');
            if (!tooltipEl) {
              tooltipEl = document.createElement('div');
              tooltipEl.id = 'chartjs-tooltip';
              tooltipEl.innerHTML = '<div class="body"><span class="text"></span><span class="figure"></span></div>';
              document.body.appendChild(tooltipEl);
            }

            if (tooltipModel.opacity === 0) {
              tooltipEl.style.opacity = 0;
              return;
            }
            tooltipEl.classList.remove('above', 'below', 'no-transform');
            if (tooltipModel.yAlign) {

              tooltipEl.classList.add(tooltipModel.yAlign);
            } else {
              tooltipEl.classList.add('no-transform');
            }

            if (tooltipModel.body) {
              $(tooltipEl).children().children('.text').text(tooltipModel.body[0].lines[0]);
            }

            var position = this._chart.canvas.getBoundingClientRect();

            tooltipEl.style.opacity = 1;
            tooltipEl.style.position = 'absolute';

            tooltipEl.style.fontFamily = 'Open Sans';

            tooltipEl.style.fontWeight = '700';
            tooltipEl.style.color = '#fff';
            tooltipEl.style.display = 'flex';
            tooltipEl.style.justifyContent = 'center';

            tooltipEl.style.borderRadius = '4px';
            tooltipEl.style.textAlign = 'center';
            tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
            tooltipEl.style.pointerEvents = 'none';
            tooltipEl.style.backgroundColor = '#7be37b';
            tooltipEl.style.zIndex = '100';

            if ($(window).width() <= 768) {
              tooltipEl.style.height = '22px';
              tooltipEl.style.width = '36px';
              tooltipEl.style.fontSize = 11 + 'px';
              tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX - 18 + 'px';
              tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY - 40 + 'px';
            } else {
              tooltipEl.style.fontSize = 16 + 'px';
              tooltipEl.style.height = '35px';
              tooltipEl.style.width = '56px';
              tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX - 28 + 'px';
              tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY - 60 + 'px';
            }

            tooltipEl.classList.add('tooltipChart');
          }
        },
        scales: {

          xAxes: [{

            gridLines: {
              display: true,
						  zeroLineWidth: 1,
              zeroLineColor: "trasparent",
              drawOnChartArea: false,
              lineWidth: 0
            },
            ticks: {
              fontSize: 14,
            }
          }],
          yAxes: [{
            color: '#8d7c7c',
            gridLines: {
              lineWidth: 2,
              zeroLineWidth: 1,
            },
            scaleLabel: {
              display: true,
            },
            ticks: {
              padding: 15,
              min: 0,
              max: max_item,
              stepSize: step,
              fontSize: 14
            }
          }]
        }
      }
    });
    let desctopOptions = [];
    desctopOptions['borderWidth'] = 2;
    desctopOptions['pointRadius'] = 7;
    desctopOptions['pointHoverRadius'] = 7;
    desctopOptions['pointHitRadius'] = 7;
    desctopOptions['fontSizeAxes'] = 14;
    desctopOptions['paddingTicks'] = 15;
    desctopOptions['gridLinesLineWidthYAxes'] = 2;
    let options = [];
    options['borderWidth'] = 1;
    options['pointRadius'] = 3;
    options['pointHoverRadius'] = 4;
    options['pointHitRadius'] = 3;
    options['fontSizeAxes'] = 10;
    options['paddingTicks'] = 10;
    options['gridLinesLineWidthYAxes'] = 1;
    if ($(window).width() <= 768) {
      updateSize(chart, options);
    }
    $(window).on('resize', function () {
      if ($(window).width() <= 768) {
        updateSize(chart, options);
      } else {
        updateSize(chart, desctopOptions);

      }
    });


    $('.chart__flat-btn').on('click', function () {
      $('.chart__flat-btn_active').removeClass('chart__flat-btn_active');
      $(this).addClass('chart__flat-btn_active');
      let flat = $(this).attr('data-item');
      let Newdata = chartData[flat];
      updateData(chart, Newdata);
    });

    function updateSize(chart, options) {

      chart.data.datasets[0].borderWidth = options['borderWidth'];
      chart.data.datasets[0].pointRadius = options['pointRadius'];
      chart.data.datasets[0].pointHoverRadius = options['pointHoverRadius'];
      chart.data.datasets[0].pointHitRadius = options['pointHitRadius'];
      chart.options.scales.xAxes[0].ticks.fontSize = options['fontSizeAxes'];
      chart.options.scales.xAxes[0].ticks.major.fontSize = options['fontSizeAxes'];
      chart.options.scales.xAxes[0].ticks.minor.fontSize = options['fontSizeAxes'];
      chart.options.scales.yAxes[0].ticks.fontSize = options['fontSizeAxes'];
      chart.options.scales.yAxes[0].ticks.major.fontSize = options['fontSizeAxes'];
      chart.options.scales.yAxes[0].ticks.minor.fontSize = options['fontSizeAxes'];
      chart.options.scales.yAxes[0].ticks.padding = options['paddingTicks'];
      chart.options.scales.yAxes[0].gridLines.lineWidth = options['gridLinesLineWidthYAxes'];
      chart.update();
    }

    function updateData(chart, Newdata) {
      let length = chart.data.datasets[0].data.length;
      for (let i = 0; i < length; i++) {
        chart.data.datasets[0].data.pop();
      }
      for (let i = 0; i < Newdata.length; i++) {
        chart.data.datasets[0].data.push(Newdata[i]);
      }
      chart.update();
    }
    Chart.pluginService.register({
      afterDraw: function (chart, easing) {
        if (chart.config.options && chart.config.options.scales) {
          if (chart.config.options.scales.xAxes)
            chart.config.options.scales.xAxes.forEach(function (xAxisConfig) {
              if (!xAxisConfig.color)
                return;

              var ctx = chart.chart.ctx;
              var chartArea = chart.chartArea;
              var xAxis = chart.scales[xAxisConfig.id];

              // just draw the scale again with different colors
              var color = xAxisConfig.gridLines.color;
              xAxisConfig.gridLines.color = xAxisConfig.color;
              xAxis.draw(chartArea);
              xAxisConfig.gridLines.color = color;
            });

          if (chart.config.options.scales.yAxes)
            chart.config.options.scales.yAxes.forEach(function (yAxisConfig) {
              if (!yAxisConfig.color)
                return;

              var ctx = chart.chart.ctx;
              var chartArea = chart.chartArea;
              var yAxis = chart.scales[yAxisConfig.id];

              // here, since we also have the grid lines, set a clip area for the left of the y axis
              ctx.save();
              ctx.rect(0, 0, chartArea.left + yAxisConfig.gridLines.lineWidth - 1, chartArea.bottom + yAxisConfig.gridLines.lineWidth - 1);
              ctx.clip();

              var color = yAxisConfig.gridLines.color;
              yAxisConfig.gridLines.color = yAxisConfig.color;
              yAxis.draw(chartArea);
              yAxisConfig.gridLines.color = color;

              ctx.restore();
            });

          // we need to draw the tooltip so that it comes over the (redrawn) elements
          chart.tooltip.transition(easing).draw();
        }
      }
    });
    }
  });
  
   function getFilterList(arrayPhotos, filter) { //Получить отфильтрованный список
      let tempList = [];
      for (let i = 0; i < arrayPhotos.length; i++) {
        if (arrayPhotos[i].year == filter.year && arrayPhotos[i].month.toLowerCase() == filter.month.toLowerCase()) {
          tempList.push(arrayPhotos[i]);
        }
      }
      
      return tempList;
    }
    
  function updateGalery(listObjects, parent, begin) {
      
      let countVisible = 4;
    //console.log(listObjects);
      var title = $('h1');  
      $('.progress__btn.show__btn').removeClass('hidden');
      $('.progress__wrapper .nothing').addClass('hidden');
      let strHTML = '';
      if ($(parent).hasClass('galery__slider')) {
        $('.galery__slider').html('');
        for (let i = 0; i < listObjects.length; i++) {
          strHTML += '<div class="galery__item"><div class="galery__item-img"><span class="close">&#10006;</span><img src="' + listObjects[i].photoLarge + '" title="Ход строительства '+$(title[0]).text()+'" alt="Ход строительства '+$(title[0]).text()+'"></div></div>';
        }
        $('.galery__slider').html(strHTML);

      } else {
        $('.build__box').html('');
        let j = 0;
        for (let i = 0; i < listObjects.length;  i++) {
           var classbox = 'build__box-img-wrap swiper-wrapper';
           if(i > 3) classbox += ' non_visible'; 
          j++;
         // strHTML += '<li class="progress__gallery-item modal-image__item"><picture><source type="image/webp" srcset=""><img class="progress__image" src="' + listObjects[i].photo + '" data-big-src="' + listObjects[i].photoLarge + '" data-index="'+i+'" title="Ход строительства '+$(title[0]).text()+'" alt="Ход строительства '+$(title[0]).text()+'" width="310" height="180"></picture></li>';
         strHTML +='<div class="'+ classbox +'"><img class="build__box-img" src="'+listObjects[i].photo+'" alt="Изображение стройки дома"></div>';
        }
        if (j == 0) {
          $('.progress__btn.show__btn').addClass('hidden');
          $('.progress__wrapper .nothing').removeClass('hidden');
        }
        $(parent).append(strHTML);
        
        $('.build__box').slick({
            autoplay: true,
            autoplaySpeed: 3000,
            adaptiveHeight: false,
            arrows: false,
            mobileFirst: true,
            responsive: [{
              breakpoint: 1023,
              settings: 'unslick'
            }
              ]
          });
          
          
        /*  new Swiper(".class_box_slider", {
                loop: !0,
                slidesPerView: 1,
                spaceBetween: 15,
                navigation: {
                    nextEl: ".house__slider-btn-next",
                    prevEl: ".house__slider-btn-prev"
                },
                breakpoints: {
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 30
                    },
                    1100: {
                        slidesPerView: 3,
                        spaceBetween: 30
                    }
                }
            })*/
    
      }

      $('.progress__image').off('click');
      $('.progress__image').on('click', clickImg);
      if ($(parent).hasClass('galery__slider')) {
        let $slider = $('.galery__slider');
        initSlidersModalPhoto($slider, begin);//функция из modal-photo-galery.js
      }
  

      function clickImg() {
        let begin = $(this).attr('data-index');
        updateGalery(filterList, $('.galery__slider'), begin);
        $('.modal-photo-galery').addClass('modal-photo-galery--active');
      }
  }
  
  (function () {
  $(document).ready(function () {
    let filter = { //Фильтр фотографий
      year: '',
      month: ''
    };

    $year = $('#build-year').val();
    $month = $('#build-month').val();
    filter.year = $year;
    filter.month = $month;
    let filterList = getFilterList(arrayPhotos, filter); //Получаем отфильтрованный список

    let $parent = $('.build__box');
    updateGalery(filterList, $parent);


    $('#build-year, #build-month').on('change', function () {
      $year = $('#build-year').val();
      $month = $('#build-month').val();

      filter.year = $year;
      filter.month = $month;
      filterList = getFilterList(arrayPhotos, filter); //Получаем отфильтрованный список
      if ($('.build__box').length) {
        $('.build__box').slick('unslick');
      }
      updateGalery(filterList, $parent);
    });
    $('.build__btn-more').click(function () {
      updateGalery(filterList, $('.galery__slider'));
      $('.modal-photo-galery').addClass('modal-photo-galery--active');
    });

  });
})();

//$(document).ready(function () {
/*var mySwiper = new Swiper(".house__slider", {
        loop: !0,
        slidesPerView: 1,
        spaceBetween: 15,
        navigation: {
            nextEl: ".house__slider-btn-next",
            prevEl: ".house__slider-btn-prev"
        },
        breakpoints: {
            768: {
                slidesPerView: 2,
                spaceBetween: 30
            },
            1100: {
                slidesPerView: 3,
                spaceBetween: 30
            }
        }
    })*/





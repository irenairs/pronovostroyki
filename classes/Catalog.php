<?php

namespace Example;
use Imagick;

/**
 * Description of Catalog
 *
 * @author iren_
 */
class Catalog {
    
    private $params = false;
    
    public function set_params($params){
        $this->params = $params;
    }

    public function get_api($page=false, $type=false){
        error_reporting(E_ALL);
        $res = [];
        $post = false;
        $cyr = new CyrToLat();
        $url = "https://joywork.ru/api/v1/catalog";
        if($page){
            
            $url = "https://joywork.ru/api/v1/catalog/".$page;
            if($type){
                $url = "https://joywork.ru/api/v1/catalog/".$type."/".$page;
                
                
            }
            //echo $url;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result   
        
        if($this->params){
            $post = true;
            $postData = '';
           // var_dump($this->params);
            foreach($this->params as $k => $v) 
            { 
               $postData .= $k . '='.$v.'&'; 
            }
            $postData = rtrim($postData, '&');
            
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        }

        // Fetch and return content, save it.
        $raw_data = curl_exec($ch);
        //var_dump($raw_data);
        curl_close($ch);
       /*if($this->params){
            var_dump($raw_data);
        }*/
        // If the API is JSON, use json_decode.
        $data = json_decode($raw_data);

        $res['blocks_count'] = $data->blocks_count;

        $res['blocks'] = $data->blocks;

        if(!$post){
            $res['developer'] = $data->developer; 

            if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/images")) {
                mkdir($_SERVER['DOCUMENT_ROOT'] . "/images");
            }
            if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/images/developer")) {
                mkdir($_SERVER['DOCUMENT_ROOT'] . "/images/developer");
            }
            foreach($data->developer as $key => $dev){
                $name = basename($dev->logo);

                if(empty($name) || $name == 'builder'){
                    $res['developer']->$key->logo = '';
                } else {

                    if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/images/developer/".$key)) {
                        mkdir($_SERVER['DOCUMENT_ROOT'] . "/images/developer/".$key);
                    }
                    $nameThumbs = '/images/developer/'.$key . '/' . $name;
                    $info = pathinfo($nameThumbs);
                    if(!file_exists($_SERVER['DOCUMENT_ROOT'].$nameThumbs)){
                        //$thumb->createThumbnail($dev->logo, $_SERVER['DOCUMENT_ROOT'].$nameThumbs, 200);

                        $im = new Imagick($dev->logo);
                        if(strpos($name, '.png') !== false){
                            $im->thumbnailImage(200,null);
                        } else if(strpos($name,'jpeg') !== false){
                            //$im->setbackgroundcolor('rgb(64, 64, 64)');
                            $im->thumbnailImage(200, 100, true, true);
                        } else {

                            $im->thumbnailImage(200,null,0);
                        }
                        $im->writeImage($_SERVER['DOCUMENT_ROOT'].$nameThumbs);
                        $im = new Imagick($_SERVER['DOCUMENT_ROOT'].$nameThumbs);
                        $im->writeImage($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'webp');
                    }
                    $res['developer']->$key->logo = $nameThumbs;
                    $res['developer']->$key->logo_webp = $info['dirname'] . '/' . $info['filename'] . '.' . 'webp';
                } 

            }
        }


        $res['block_images'] = $data->block_images;
        foreach($data->block_images as $key => $imgArr){

            $tempArr=array();
            $timgArr = array();
            if(empty($imgArr)){
               $tempArr['img'] = '/assets/img/No_image_available.svg';
            } else {
                if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/images/".$key)) {
                    mkdir($_SERVER['DOCUMENT_ROOT'] . "/images/".$key);
                }
                foreach ($imgArr as $img){
                    $name = basename($img);
                    $nameThumbs = '/images/'.$key . '/' . str_replace('.', '_600.',$name);

                    $info = pathinfo($nameThumbs);
                    if(!file_exists($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg')){

                        $im = new Imagick($img);
                        $im->thumbnailImage(600,null,0);
                        $im->setImageCompression(Imagick::COMPRESSION_JPEG);
                        $im->setImageCompressionQuality(75);
                        $im->stripImage();
                        $im->writeImage($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg');
                        $im = new Imagick($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg');
                        $im->writeImage($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'webp');
                    }
                    $tempArr['img'] = $info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg';
                    $tempArr['webp'] = $info['dirname'] . '/' . $info['filename'] . '.' . 'webp';
                    $timgArr[] = $tempArr;
                }
            }
            $res['block_images']->$key = $timgArr;
        }
        $res['block_corpuses'] = $data->block_corpuses;
        $res['apartments'] = $data->apartments;
        $res['block_benefits'] = $data->block_benefits;
        $classListBenefits = array('persent.jpg'=> 'sale', 
            'ico_input.jpg'=> 'gift',
            'check.jpg'=> 'gift',
            'ico_cert.jpg'=> 'decor',
            'paper.jpg'=> 'decor',
            'ico_spec.jpg'=> 'conditions',
            'ico-bus.jpg'=> 'conditions'
            
          );
        
        if(!$post){
            $res['metro_all'] = $data->metro_all;
            if($type && $type == 'metro'){
                foreach($data->metro_all as $metro){
                    $metro_name = $cyr->convert($metro -> name);
                    if($metro_name == $page){
                        $res['metro_id'] = $metro ->id;
                        $res['metro_name'] = $metro ->name;
                    }
                }
                $page = 1;
            }
            $res['regions'] = $data->regions;
            if($type && $type == 'region'){
                foreach($data->regions as $region){
                    $region_name = $cyr->convert(trim(str_replace('р-н','', $region -> name)));
                    if($region_name == $page){
                        $res['region_id'] = $region -> id;
                        $res['region_name'] = trim(str_replace('р-н','', $region -> name));
                    }
                }
                $page = 1;
            }
            
            $res['room_types'] = $data->room_types;
            $res['banks'] = $data->banks;
            $res['class'] = $data->class;
            $res['decoration'] = $data->decoration;
            $res['classListBenefits'] = $classListBenefits;
            $res['page'] = $page;
        }
        return $res;
    }
}

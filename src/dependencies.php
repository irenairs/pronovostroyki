<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// mail
$container['transport'] = function ($c) {
	// Create the Transport
	// $settings = $c->get('settings')['transport'];
	$transport = (new Swift_SmtpTransport('smtp.mailgun.org', 587, 'tls'))
	->setUsername('robot@joywork.ru')
    ->setPassword('Niut3swec[');
	// Create the Mailer using your created Transport
	$mailer = new Swift_Mailer($transport);
	return $mailer;
};

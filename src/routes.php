<?php

error_reporting(0);
use Example\CyrToLat;
use Example\ThumbsCreate;
use Example\Catalog;
// Routes

$app->get('/', function ($request, $response, $args) {
    $res = [];
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, "https://joywork.ru/api/v1/main");
    curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result   

    // Fetch and return content, save it.
    $raw_data = curl_exec($ch);
    //var_dump($raw_data);
    curl_close($ch);

	// If the API is JSON, use json_decode.
    $data = json_decode($raw_data);
    $res['blocks_count'] = $data->blocks_count;
    
    foreach($data->blocks_count as $index => $analog){
        $img = $analog->image;
        $key = (int)$analog->id;
        $name = basename($img);
        $nameThumbs = '/images/'.$key . '/' . str_replace('.', '_450.',$name);
        $info = pathinfo($nameThumbs);
        if(!file_exists($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg')){
            $im = new Imagick($img);
            $im->thumbnailImage(450,null,0);
            
            
            //$im->setImageFormat('jpeg');
            $im->setImageCompression(Imagick::COMPRESSION_JPEG);
            $im->setImageCompressionQuality(75);
            $im->stripImage();
            $im->writeImage($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg');
            
        }
        $res['blocks_count'][$index]->image = $info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg';
            
    }
    
    $res['block_images'] = $data->block_images;
        foreach($data->block_images as $key => $imgArr){

            $tempArr=array();
            $timgArr = array();
            if(empty($imgArr)){
               $tempArr['img'] = '/assets/img/No_image_available.svg';
            } else {
                if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/images/".$key)) {
                    mkdir($_SERVER['DOCUMENT_ROOT'] . "/images/".$key);
                }
                foreach ($imgArr as $img){
                    $name = basename($img);
                    $nameThumbs = '/images/'.$key . '/' . str_replace('.', '_600.',$name);

                    $info = pathinfo($nameThumbs);
                    if(!file_exists($_SERVER['DOCUMENT_ROOT'].$nameThumbs)){

                        $im = new Imagick($img);
                        $im->thumbnailImage(600,null,0);
                        $im->writeImage($_SERVER['DOCUMENT_ROOT'].$nameThumbs);
                        $im = new Imagick($_SERVER['DOCUMENT_ROOT'].$nameThumbs);
                        $im->writeImage($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'webp');
                    }
                    $tempArr['img'] = $nameThumbs;
                    $tempArr['webp'] = $info['dirname'] . '/' . $info['filename'] . '.' . 'webp';
                    $timgArr[] = $tempArr;
                }
            }
            $res['block_images']->$key = $timgArr;
        }
    
    $res['block_benefits'] = $data->block_benefits;
    $res['block_corpuses'] = $data->block_corpuses;
    $res['apartments'] = $data->apartments;
    $res['developer'] = $data->developer;
    $res['metro_all'] = $data->metro_all;
    $res['regions'] = $data->regions;
    $res['room_types'] = $data->room_types;
    $res['banks'] = $data->banks;
    $res['class'] = $data->class;
    $res['decoration'] = $data->decoration;
    $res['blocks'] = $data->blocks;
    $res['videos'] = $data->videos;
    
    return $this->renderer->render($response, 'index.phtml', $res);
});


$app->get('/zhk/[{id}]', function ($request, $response, $args) {
	
    $cyr = new CyrToLat();
    $res = [];

	// get block images
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://joywork.ru/api/v1/block/".$args['id']);
	curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result   

	// Fetch and return content, save it.
	$raw_data = curl_exec($ch);
	curl_close($ch);

	// If the API is JSON, use json_decode.
	$data = json_decode($raw_data);
        if(empty($data)){
            header("HTTP/1.0 404 Not Found");
            header("HTTP/1.1 404 Not Found");
            header("Status: 404 Not Found");
            return $this->renderer->render($response, '404.phtml');
            exit();
        } 
        
        $imgSize = [100, 300, 950];
	
	$res['block_entry'] = $data->block_entry;
        $res['block_entry']->reg_name = $cyr->convert(trim(str_replace('р-н','', $data->block_entry->reg_name)));
        $res['block_entry']->metro = $cyr->convert(trim($data->block_add->name));
	$res['block_images'] = array();
        
        foreach($data->block_images as $key => $image){
           
            $name= basename($image);
            $arrImg = array();
            foreach($imgSize as $size){
                $nameThumbs = '/images/'.str_replace('.', '_'.$size.'.', $name);
                $info = pathinfo($nameThumbs);
                if(!file_exists($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg')){
                    $im = new Imagick($image);
                    $im->thumbnailImage($size,null,0);
                        $im->setImageCompression(Imagick::COMPRESSION_JPEG);
                    $im->setImageCompressionQuality(75);
                    $im->stripImage();
                    $im->writeImage($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg');
                }
                
                if($size == 100){
                    $arrImg['low'] = $info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg';
                } else if ($size == 300){
                    $arrImg['medium'] = $info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg';
                } else if ($size == 950){
                    $arrImg['big'] = $info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg';
                }
                
            }
            $res['block_images'][] = $arrImg;
        }
        
	$res['builder_info'] = $data->builder_info;
        $logo_builder = $data->builder_info->logo;
        //var_dump($logo_builder);
        if(!empty($logo_builder)){
            $name = 'orig_'.basename($logo_builder);
            //echo $name;
            $path = "/images/developer/".$data->builder_info->id;
            //echo $path;
            if (!is_dir($_SERVER['DOCUMENT_ROOT'] . $path)) {
                mkdir($_SERVER['DOCUMENT_ROOT'] . $path);
            }
            if(!file_exists($_SERVER['DOCUMENT_ROOT'].$path.'/'.$name)){
                try{
                    $im = new Imagick($logo_builder);
                    $w = $im->getImageWidth();
                    $h = $im->getImageHeight();    
                    $im->thumbnailImage($w, $h, true, true);

                    $im->writeImage($_SERVER['DOCUMENT_ROOT'].$path.'/'.$name);
                } catch (Exception $e){}
                     
            }
            $res['builder_info']->logo = $path.'/'.$name;
        }
        $res['corpuses_info'] = $data->corpuses_info;
        $res['apartments'] = $data->apartments;
        $res['block_videos'] = $data->block_videos;
        $res['block_video_images'] = array();
        if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/images")) {
            mkdir($_SERVER['DOCUMENT_ROOT'] . "/images");
        }



        if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/images/".$data->block_entry->id)) {
            mkdir($_SERVER['DOCUMENT_ROOT'] . "/images/".$data->block_entry->id);
        }
        //var_dump($data->block_video_images);
        
        foreach($data->block_video_images as $key => $image){
            //var_dump($image);
            if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/images/".$data->block_entry->id."/".$image->id)) {
                mkdir($_SERVER['DOCUMENT_ROOT'] . "/images/".$data->block_entry->id."/".$image->id);
            }
            
            $name= basename($image->photo);
            $arrImg = array();
            foreach($imgSize as $size){
                $nameThumbs = "/images/".$data->block_entry->id."/".$image->id."/".str_replace('.', '_'.$size.'.', $name);
                $info = pathinfo($nameThumbs);
                if(!file_exists($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg')){
                    $im = new Imagick($image->photo);
                    $im->thumbnailImage($size,null,0);
                        $im->setImageCompression(Imagick::COMPRESSION_JPEG);
                    $im->setImageCompressionQuality(75);
                    $im->stripImage();
                    $im->writeImage($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg');
                }
                if($size == 100){
                    $arrImg['low'] = $info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg';
                } else if ($size == 300){
                    $arrImg['medium'] = $info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg';
                } else if ($size == 950){
                    $arrImg['big'] = $info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg';
                }
                $res['block_video_images'][$key] = $arrImg;
            }
        }
        
        $res['block_real_images'] = array();
        
        foreach($data->block_real_images as $key => $image){
           
            $name= basename($image);
            $arrImg = array();
            foreach($imgSize as $size){
                $nameThumbs = '/images/'.str_replace('.', '_'.$size.'.', $name);
                $info = pathinfo($nameThumbs);
                if(!file_exists($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg')){
                    $im = new Imagick($image);
                    $im->thumbnailImage($size,null,0);
                    $im->setImageCompression(Imagick::COMPRESSION_JPEG);
                    $im->setImageCompressionQuality(75);
                    $im->stripImage();
                    $im->writeImage($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg');
                }
                if($size == 100){
                    $arrImg['low'] = $info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg';
                } else if ($size == 300){
                    $arrImg['medium'] = $info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg';
                } else if ($size == 950){
                    $arrImg['big'] = $info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg';
                }
                
            }
            $res['block_real_images'][] = $arrImg;
        }
        
        $res['block_features'] = $data->block_features;
        $res['block_author'] = $data->block_author;
        if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/images/author")) {
            mkdir($_SERVER['DOCUMENT_ROOT'] . "/images/author");
        }
        $img_autor = basename($data->block_author->photo);
        $pathAytor = "/images/author/".$img_autor;
        if(!file_exists($_SERVER['DOCUMENT_ROOT'] . $pathAytor)){
            try{
                $im = new Imagick($data->block_author->photo);
                
                $w = $im->getImageWidth();
                $h = $im->getImageHeight();
                         
                $im->thumbnailImage($w, $h, true, true);

                $im->writeImage($_SERVER['DOCUMENT_ROOT'].$pathAytor);
            } catch(Exception $e) {
                        //echo $e;
            }
        }
        $res['block_author']->photo = $pathAytor;
        $res['block_chart_labels'] = $data->block_chart_labels;
        $res['block_sales'] = $data->block_sales;
        $res['block_documents'] = $data->block_documents;
        $res['block_progress_images'] = $data->block_progress_images;
        
        foreach($data->block_progress_images as $key => $image){
            $namePoto = $image->photo;
            $name= basename($image->photo);
            foreach($imgSize as $size){
                $nameThumbs = '/images/'.str_replace('.', '_'.$size.'.', $name);
                $info = pathinfo($nameThumbs);
                if(!file_exists($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg')){
                    $im = new Imagick($namePoto);
                    $im->thumbnailImage($size,null,0);
                    $im->setImageCompression(Imagick::COMPRESSION_JPEG);
                    $im->setImageCompressionQuality(75);
                    $im->stripImage();
                    $im->writeImage($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg');
                }
                if($size == 300) $res['block_progress_images'][$key]->photo = $info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg';
                if($size == 950) $res['block_progress_images'][$key]->photoLarge = $info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg';
            }

        }
        
        $res['apartments_list'] = array();
        foreach($data->apartments_list as $ap){
            //$temp_arr = array();
            if(!isset($res['apartments_list'][$ap->room])) $res['apartments_list'][$ap->room] = array();
            
            $name = basename($ap->imgPlan);
            if(empty($name)){
                $ap->imgPlan = "/assets/img/No_image_available.svg";
            } else {
                $path = "/images/apartments";
                //echo $path;
                if (!is_dir($_SERVER['DOCUMENT_ROOT'] . $path)) {
                    mkdir($_SERVER['DOCUMENT_ROOT'] . $path);
                }
                $path .= "/".$ap->fid;
                if (!is_dir($_SERVER['DOCUMENT_ROOT'] . $path)) {
                    mkdir($_SERVER['DOCUMENT_ROOT'] . $path);
                }
                $info = pathinfo($path.'/'.$name);
                if(!file_exists($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg')){
                    try{
                        $im = new Imagick($ap->imgPlan);
                        $w = $im->getImageWidth();
                        $h = $im->getImageHeight();
                        if($w > 556) {
                            $w = 556;
                            $delta = $w/$h;
                            $h = $h * $delta;
                        } 
                        if($h > 412){
                            $h = 412;
                            $delta = $h/$w;
                            $w = $w * $delta;
                        }
                            
                        $im->thumbnailImage($w, $h, true, true);
                        $im->setImageCompression(Imagick::COMPRESSION_JPEG);
                        $im->setImageCompressionQuality(75);
                        $im->stripImage();
                        $im->writeImage($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg');
                    } catch(Exception $e) {
                        //echo $e;
                    }

                }
                $ap->imgPlan = $info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg';
            }
            $res['apartments_list'][$ap->room][] = $ap;
            
        }
        
        //$res['apartments_list'] = $data->apartments_list;
        $res['block_benefits'] = $data->block_benefits;
        $res['block_banks'] = array();
        if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/images/banks")) {
            mkdir($_SERVER['DOCUMENT_ROOT'] . "/images/banks");
        }
        $i = 0;
        foreach($data->block_banks as $key => $bank){
            if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/images/banks/".$bank->id)) {
                mkdir($_SERVER['DOCUMENT_ROOT'] . "/images/banks/".$bank->id);
            }
            $logo = basename($bank->img);
            $path = "/images/banks/".$bank->id."/".$logo;
            if(!file_exists($_SERVER['DOCUMENT_ROOT'].$path.'/'.$name)){
                try{
                    $im = new Imagick($bank->img);
                    $w = $im->getImageWidth();
                    $h = $im->getImageHeight();
                            
                    $im->thumbnailImage($w, $h, true, true);

                    $im->writeImage($_SERVER['DOCUMENT_ROOT'].$path);
                    $bank->img = $path;
                } catch(Exception $e) {
                        //echo $e;
                }
            }
            if(!isset($res['block_banks'][$i])) $res['block_banks'][$i] = array();
            $res['block_banks'][$i][] = $bank;
            if(($key+1) % 4 == 0){
                $i++;
            }
        }
        $res['block_banks_total'] = count($data->block_banks);
        $res['block_add'] = $data->block_add;
       // $res['svg']=$svg;
        $res['decoration'] = $data->decoration;
        $res['corpuses'] = $data->corpuses;
        $res['types'] = $types = [
            0 => '-',
            1 => 'ДДУ',
            2 => 'ЖСК',
            3 => 'КП',
            4 => 'ПДКП',
        ];
        $res['block_reviews'] = $data->block_reviews;
        $res['builder_reviews'] = $data->builder_reviews;
        $res['analog_blocks'] = $data->analog_blocks;
        foreach($data->analog_blocks as $index => $analog){
            $img = $analog->image;
            $key = (int)$analog->id;
            $name = basename($img);
            $nameThumbs = '/images/'.$key . '/' . str_replace('.', '_450.',$name);
            $info = pathinfo($nameThumbs);
            if(!file_exists($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg')){
                $im = new Imagick($img);
                $im->thumbnailImage(450,null,0);
                $im->setImageCompression(Imagick::COMPRESSION_JPEG);
                $im->setImageCompressionQuality(75);
                $im->stripImage();
                $im->writeImage($_SERVER['DOCUMENT_ROOT'].$info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg');
            }
            $res['analog_blocks'][$index]->image = $info['dirname'] . '/' . $info['filename'] . '.' . 'jpeg';
            
        }
        $res['plus_minus'] = $data->plus_minus;
        $res['metas'] = $data->metas;
    // Render index view
    return $this->renderer->render($response, 'details.phtml', $res);
        
});



$app->post('/consultation', function ($request, $response, $args) {
	$allPostPutVars = $request->getParsedBody();
        $text = '<p><b>Получить консультацию юриста</b></p>';
	foreach($allPostPutVars as $key => $param){
	   $res[$key] = $param;
           if($key == 'fio'){
               $text .= "<p>Имя пользователя: ".$param."</p>";
           }
           if($key == 'phone'){
               $text .= "<p>Телефон пользователя: +7".$param."</p>";
           }
           if($key == 'blockName'){
               $text .= "<p>".$param."</p>";
           }
	}
        $res['subject'] = "consultation";
        
        
        $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://joywork.ru/api/v1/message");
	curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "json=". json_encode($res, JSON_UNESCAPED_UNICODE));
        

	// Fetch and return content, save it.
	$raw_data = curl_exec($ch);
	curl_close($ch);
        
	$message = (new Swift_Message('Клиент с ПроНовостройки'))
	  ->setFrom(['pronovostroyki.mail@gmail.com' => 'pronovostroykimail'])
	  ->setTo(['Ellada-an@mail.ru','sndmitrievsn@mail.ru','JoyWork2016@mail.ru'])
	  ->setBody($text, 'text/html');
	$result = $this->transport->send($message);
	return $response->withJson($res);
        
        
        
})->setName('consultation');

$app->post('/mortgage', function ($request, $response, $args) {
	$allPostPutVars = $request->getParsedBody();
        $text = '<p><b>Подать заявки во все банки</b></p>';
	foreach($allPostPutVars as $key => $param){
	   $res[$key] = $param;
           if($key == 'fio'){
               $text .= "<p>Имя пользователя: ".$param."</p>";
           }
           if($key == 'phone'){
               $text .= "<p>Телефон пользователя: +7".$param."</p>";
           }
           if($key == 'blockName'){
               $text .= "<p>".$param."</p>";
           }
           if($key == 'income'){
               $text .= "<p>Месячный доход семьи: ".$param." руб.</p>";
               
           }
           
           if($key == 'work'){
               $text .= "<p>Общий трудовой стаж: ".$param." (лет)</p>";
           }
           if($key == 'payment'){
               $text .= "<p>Первоначальный взнос: ".$param." руб.</p>";
           }
           
           if($key == 'capital'){
               $capital = "Нет";
               if($param == 1) $capital = "Да";
               $text .= "<p>Наличие материнского капитала и субсидий: ".$capital."</p>";
           }
	}
        $res['subject'] = "mortgage";
        
        
        $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://joywork.ru/api/v1/message");
	curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "json=". json_encode($res, JSON_UNESCAPED_UNICODE));
        

	// Fetch and return content, save it.
	$raw_data = curl_exec($ch);
	curl_close($ch);
        
        
	$message = (new Swift_Message('Клиент с ПроНовостройки'))
	  ->setFrom(['pronovostroyki.mail@gmail.com' => 'pronovostroykimail'])
	  ->setTo(['Ellada-an@mail.ru','sndmitrievsn@mail.ru','JoyWork2016@mail.ru'])
	  ->setBody($text, 'text/html');
	$result = $this->transport->send($message);
	return $response->withJson($res);
        
        
        
})->setName('mortgage');

$app->post('/tobook', function ($request, $response, $args) {
	$allPostPutVars = $request->getParsedBody();
        $text = '<p><b>Забронироваать и получить дополнительную скидку</b></p>';
	foreach($allPostPutVars as $key => $param){
	   $res[$key] = $param;
           if($key == 'fio'){
               $text .= "<p>Имя пользователя: ".$param."</p>";
           }
           if($key == 'phone'){
               $text .= "<p>Телефон пользователя: +7".$param."</p>";
           }
           if($key == 'blockName'){
               $text .= "<p>".$param."</p>";
           }
           if($key == 'flatId'){
               $text .= "<p>ID квартиры: ".$param."</p>";
           }
           
	}
        $res['subject'] = "tobook";
        
        
        $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://joywork.ru/api/v1/message");
	curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "json=". json_encode($res, JSON_UNESCAPED_UNICODE));
        

	// Fetch and return content, save it.
	$raw_data = curl_exec($ch);
	curl_close($ch);
        
        
	$message = (new Swift_Message('Клиент с ПроНовостройки'))
	  ->setFrom(['pronovostroyki.mail@gmail.com' => 'pronovostroykimail'])
	  ->setTo(['Ellada-an@mail.ru','sndmitrievsn@mail.ru','JoyWork2016@mail.ru'])
	  ->setBody($text, 'text/html');
	$result = $this->transport->send($message);
	return $response->withJson($res);
        
        
        
})->setName('tobook');

$app->post('/excursion', function ($request, $response, $args) {
	$allPostPutVars = $request->getParsedBody();
        $text = '<p><b>Записаться на экскурсию</b></p>';
	foreach($allPostPutVars as $key => $param){
	   $res[$key] = $param;
           if($key == 'fio'){
               $text .= "<p>Имя пользователя: ".$param."</p>";
           }
           if($key == 'phone'){
               $text .= "<p>Телефон пользователя: +7".$param."</p>";
           }
           if($key == 'blockName'){
               $text .= "<p>".$param."</p>";
           }
           if($key == 'flatId'){
               $text .= "<p>ID квартиры: ".$param."</p>";
           }
           if($key == 'date'){
               $text .= "<p>Дата: ".$param."</p>";
           }
           if($key == 'timeex'){
               $text .= "<p>Время: ".$param."</p>";
           }
           
	}
        $res['subject'] = "excursion";
        
        
        $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://joywork.ru/api/v1/message");
	curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "json=". json_encode($res, JSON_UNESCAPED_UNICODE));
        

	// Fetch and return content, save it.
	$raw_data = curl_exec($ch);
	curl_close($ch);
        
        
	$message = (new Swift_Message('Клиент с ПроНовостройки'))
	  ->setFrom(['pronovostroyki.mail@gmail.com' => 'pronovostroykimail'])
	  ->setTo(['Ellada-an@mail.ru','sndmitrievsn@mail.ru','JoyWork2016@mail.ru'])
	  ->setBody($text, 'text/html');
	$result = $this->transport->send($message);
	return $response->withJson($res);
        
        
        
})->setName('excursion');

$app->post('/review', function ($request, $response, $args) {
	$res = [];
        $postData = '';
        //create name value pairs seperated by &
        foreach($request->getParsedBody() as $k => $v) 
        { 
           $postData .= $k . '='.$v.'&'; 
        }
        $postData = rtrim($postData, '&');
	// get block images
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://joywork.ru/api/v1/review");
	curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        

	// Fetch and return content, save it.
	$raw_data = curl_exec($ch);
	curl_close($ch);
        var_dump($raw_data);
});

$app->post('/review_builder', function ($request, $response, $args) {
	$res = [];
       // var_dump($request->getParsedBody());
        
        $postData = '';
        //create name value pairs seperated by &
        foreach($request->getParsedBody() as $k => $v) 
        { 
           $postData .= $k . '='.$v.'&'; 
        }
        $postData = rtrim($postData, '&');
	// get block images
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://joywork.ru/api/v1/review_builder");
	curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        

	// Fetch and return content, save it.
	$raw_data = curl_exec($ch);
	curl_close($ch);
        var_dump($raw_data);
});

$app->post('/tocall', function ($request, $response, $args) {
	$allPostPutVars = $request->getParsedBody();
        $text = '<p><b>Заявка на звонок</b></p>';
	foreach($allPostPutVars as $key => $param){
	   $res[$key] = $param;
           if($key == 'fio'){
               $text .= "<p>Имя пользователя: ".$param."</p>";
           }
           if($key == 'phone'){
               $text .= "<p>Телефон пользователя: +7".$param."</p>";
           }
           if($key == 'blockName'){
               $text .= "<p>".$param."</p>";
           }
           
	}
        $res['subject'] = "tocall";
        
        
        $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://joywork.ru/api/v1/message");
	curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "json=". json_encode($res, JSON_UNESCAPED_UNICODE));
        

	// Fetch and return content, save it.
	$raw_data = curl_exec($ch);
	curl_close($ch);
        
        
	$message = (new Swift_Message('Клиент с ПроНовостройки'))
	  ->setFrom(['pronovostroyki.mail@gmail.com' => 'pronovostroykimail'])
	  ->setTo(['Ellada-an@mail.ru','sndmitrievsn@mail.ru','JoyWork2016@mail.ru'])
	  ->setBody($text, 'text/html');
	$result = $this->transport->send($message);
	return $response->withJson($res);
        
        
        
})->setName('tobook');

$app->post('/toreport', function ($request, $response, $args) {
	$allPostPutVars = $request->getParsedBody();
        $text = '<p><b>Заявка на отчет</b></p>';
	foreach($allPostPutVars as $key => $param){
	   $res[$key] = $param;
           if($key == 'fio'){
               $text .= "<p>Имя пользователя: ".$param."</p>";
           }
           if($key == 'email'){
               $text .= "<p>Электронная почта: ".$param."</p>";
           }
           if($key == 'phone'){
               $text .= "<p>Телефон пользователя: +7".$param."</p>";
           }
           if($key == 'blockName'){
               $text .= "<p>".$param."</p>";
           }
           
	}
        $res['subject'] = "report";
        
        
        $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://joywork.ru/api/v1/message");
	curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "json=". json_encode($res, JSON_UNESCAPED_UNICODE));
        

	// Fetch and return content, save it.
	$raw_data = curl_exec($ch);
	curl_close($ch);
        
        
	$message = (new Swift_Message('Клиент с ПроНовостройки'))
	  ->setFrom(['pronovostroyki.mail@gmail.com' => 'pronovostroykimail'])
	  ->setTo(['Ellada-an@mail.ru','sndmitrievsn@mail.ru','JoyWork2016@mail.ru'])
	  ->setBody($text, 'text/html');
	$result = $this->transport->send($message);
	return $response->withJson($res);
        
        
        
})->setName('toreport');

$app->get("/index", function($request, $response, $args){
    
    
    return $this->renderer->render($response, 'test.phtml');
});

$app->get("/catalog_old", function($request, $response, $args){
    
    $catalog = new Catalog();
    $res = $catalog->get_api();
    return $this->renderer->render($response, 'catalog_old.phtml', $res);
});

$app->post("/catalog", function($request, $response, $args){
    
    $catalog = new Catalog();
    $catalog->set_params($request->getParsedBody());
    $res = $catalog->get_api();
    echo json_encode($res, JSON_UNESCAPED_UNICODE);
});

$app->get("/catalog", function($request, $response, $args){
     
    $catalog = new Catalog();
    $res = $catalog->get_api();
    return $this->renderer->render($response, 'catalog_dev.phtml', $res);
});


$app->get("/catalog/metro/[{metro}]", function($request, $response, $args){
    
    $catalog = new Catalog();
    $res = $catalog->get_api($args['metro'], 'metro');
    return $this->renderer->render($response, 'catalog_dev.phtml', $res);
});

$app->get("/catalog/region/[{region}]", function($request, $response, $args){
    
    $catalog = new Catalog();
    $res = $catalog->get_api($args['region'], 'region');
    return $this->renderer->render($response, 'catalog_dev.phtml', $res);
});

$app->get("/catalog/[{id}]", function($request, $response, $args){
    //echo $args['id'];
    $catalog = new Catalog();
    $res = $catalog->get_api($args['id']);
    return $this->renderer->render($response, 'catalog_dev.phtml', $res);
});

/*$app->get('/catalog/[{id}]/[{name}]', function () use ($app) {
    $app->redirect('/catalog/[{id}]', 301);
});*/

/*$app->get("/catalog/[{id}]/", function($request, $response, $args){
    //echo $args['id'];
    $catalog = new Catalog();
    $res = $catalog->get_api($args['id']);
    return $this->renderer->render($response, 'catalog_dev.phtml', $res);
});*/

$app->get("/news/mk2020", function($request, $response, $args){
    return $this->renderer->render($response, 'news.phtml');
});

$app->get("/license", function($request, $response, $args){
    return $this->renderer->render($response, 'license.phtml');
});

$app->get("/sitemapxml", function($request, $response, $args){
    
    error_reporting(E_ALL | E_STRICT);
    ini_set('display_errors', 1);
    
    $res = [];

    // get block images
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://joywork.ru/api/v1/sitemap");
    curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result   

    // Fetch and return content, save it.
    $raw_data = curl_exec($ch);
    curl_close($ch);

    // If the API is JSON, use json_decode.
    $data = json_decode($raw_data);
     
    $file = $_SERVER['DOCUMENT_ROOT'].'/sitemap.xml';
    $tab = "\t";
    if($f = fopen($file, 'w')){
    fwrite($f, '<?xml version="1.0" encoding="UTF-8"?>'. PHP_EOL);
    fwrite($f, '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">'. PHP_EOL);
    // $url = $sm_config['url'];
    $tm = date('c', time());
    $url = 'http://';
    if(!empty($_SERVER['HTTPS'])){
        $url = 'https://';
    }
    $url .= $_SERVER['SERVER_NAME'];
    
    fwrite($f, '<url>' . PHP_EOL);
    fwrite($f, $tab . '<loc>' . $url . '</loc>' . PHP_EOL);
    fwrite($f, $tab . '<lastmod>' . $tm . '</lastmod>' . PHP_EOL);
    fwrite($f, $tab . '<priority>1.0</priority>' . PHP_EOL);
    fwrite($f, '</url>' . PHP_EOL);
    
    fwrite($f, '<url>' . PHP_EOL);
    fwrite($f, $tab . '<loc>' . $url . '/catalog</loc>' . PHP_EOL);
    fwrite($f, $tab . '<lastmod>' . $tm . '</lastmod>' . PHP_EOL);
    fwrite($f, $tab . '<priority>1.0</priority>' . PHP_EOL);
    fwrite($f, '</url>' . PHP_EOL);
    
    fwrite($f, '<url>' . PHP_EOL);
    fwrite($f, $tab . '<loc>' . $url . '/news/mk2020</loc>' . PHP_EOL);
    fwrite($f, $tab . '<lastmod>' . $tm . '</lastmod>' . PHP_EOL);
    fwrite($f, $tab . '<priority>1.0</priority>' . PHP_EOL);
    fwrite($f, '</url>' . PHP_EOL);
    
    foreach ($data->blocks as $block){
        fwrite($f, '<url>' . PHP_EOL);
        fwrite($f, $tab . '<loc>' . $url . '/zhk/' .$block->slug . '</loc>' . PHP_EOL);
        fwrite($f, $tab . '<lastmod>' . $tm . '</lastmod>' . PHP_EOL);
        fwrite($f, $tab . '<priority>1.0</priority>' . PHP_EOL);
        fwrite($f, '</url>' . PHP_EOL);
    }
    
           
    fwrite($f, '</urlset>'. PHP_EOL);
    fclose($f);
    } else {
        echo 'fail';
    }
   // }
});

$app->get("/sitemap", function($request, $response, $args){
    $res = [];

    // get block images
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://joywork.ru/api/v1/sitemap");
    curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result   

    // Fetch and return content, save it.
    $raw_data = curl_exec($ch);
    curl_close($ch);

    // If the API is JSON, use json_decode.
    $data = json_decode($raw_data);
    $res['blocks'] = $data->blocks;
    $res['metros'] = $data->metros;
    return $this->renderer->render($response, 'sitemap.phtml', $res);
});

//Override the default Not Found Handler after App
unset($app->getContainer()['notFoundHandler']);
$app->getContainer()['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        $response = new \Slim\Http\Response(404);
       $notFoundPage = file_get_contents($_SERVER['DOCUMENT_ROOT']."/404.phtml");
        return $response->write($notFoundPage);
        //return $response->write("Page not found");
    };
};

